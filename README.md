## Collaborative filtering BE

Backend application for collaborative filtering semestral project.

### Prerequisites

Java JDK 8 and up,
Maven,
Postgresql 9 and up

### Build and run

1. edit db connection in db.properties
2. compile sources with maven (pom.xml) ```mvn clean install```
3. run with ```java -jar target/cfbe-version-label.jar``` 